# PCIe transceivers

# quad 103
# refclkGTY_REFCLK_X0Y6 : 16 Gbps with 100 MHz 
set_property LOC GTY_QUAD_X0Y3 [get_cells Versal_top_i/flx182_transceivers/gt_quad_base/inst/quad_inst]
set_property LOC GTY_REFCLK_X0Y6 [get_cells  Versal_top_i/flx182_transceivers/util_ds_buf/U0/USE_IBUFDS_GTE5.GEN_IBUFDS_GTE5[0].IBUFDS_GTE5_I]
create_clock -period 10.0 [get_ports bridge_refclkGTY_REFCLK_X0Y6_diff_gt_ref_clock_clk_p[0]]
set_property PACKAGE_PIN W39 [get_ports bridge_refclkGTY_REFCLK_X0Y6_diff_gt_ref_clock_clk_p]
set_property PACKAGE_PIN W40 [get_ports bridge_refclkGTY_REFCLK_X0Y6_diff_gt_ref_clock_clk_n]


# quad 104
# refclkGTY_REFCLK_X0Y8 : 16 Gbps with 100 MHz 
set_property LOC GTY_QUAD_X0Y4 [get_cells Versal_top_i/flx182_transceivers/gt_quad_base_1/inst/quad_inst]
set_property LOC GTY_REFCLK_X0Y8 [get_cells Versal_top_i/flx182_transceivers/util_ds_buf_1/U0/USE_IBUFDS_GTE5.GEN_IBUFDS_GTE5[0].IBUFDS_GTE5_I]
create_clock -period 10.0 [get_ports bridge_refclkGTY_REFCLK_X0Y8_diff_gt_ref_clock_clk_p[0]]
set_property PACKAGE_PIN R39 [get_ports bridge_refclkGTY_REFCLK_X0Y8_diff_gt_ref_clock_clk_p]
set_property PACKAGE_PIN R40 [get_ports bridge_refclkGTY_REFCLK_X0Y8_diff_gt_ref_clock_clk_n]


# quad 105
# refclkGTY_REFCLK_X0Y10 : 16 Gbps with 100 MHz 
set_property LOC GTY_QUAD_X0Y5 [get_cells Versal_top_i/flx182_transceivers/gt_quad_base_2/inst/quad_inst]
set_property LOC GTY_REFCLK_X0Y10 [get_cells Versal_top_i/flx182_transceivers/util_ds_buf_2/U0/USE_IBUFDS_GTE5.GEN_IBUFDS_GTE5[0].IBUFDS_GTE5_I]
create_clock -period 10.0 [get_ports bridge_refclkGTY_REFCLK_X0Y10_diff_gt_ref_clock_clk_p[0]]
set_property PACKAGE_PIN L39 [get_ports bridge_refclkGTY_REFCLK_X0Y10_diff_gt_ref_clock_clk_p]
set_property PACKAGE_PIN L40 [get_ports bridge_refclkGTY_REFCLK_X0Y10_diff_gt_ref_clock_clk_n]

# quad 106
# refclkGTY_REFCLK_X0Y12 : 16 Gbps with 100 MHz
set_property LOC GTY_QUAD_X0Y6 [get_cells Versal_top_i/flx182_transceivers/gt_quad_base_3/inst/quad_inst]
set_property LOC GTY_REFCLK_X0Y12 [get_cells Versal_top_i/flx182_transceivers/util_ds_buf_3/U0/USE_IBUFDS_GTE5.GEN_IBUFDS_GTE5[0].IBUFDS_GTE5_I]
create_clock -period 10.0 [get_ports bridge_refclkGTY_REFCLK_X0Y12_diff_gt_ref_clock_clk_p[0]]
set_property PACKAGE_PIN G39 [get_ports bridge_refclkGTY_REFCLK_X0Y12_diff_gt_ref_clock_clk_p]
set_property PACKAGE_PIN G40 [get_ports bridge_refclkGTY_REFCLK_X0Y12_diff_gt_ref_clock_clk_n]


# quad 200
# refclkGTY_REFCLK_X1Y0 : 25 Gbps with 320 MHz
set_property LOC GTY_QUAD_X1Y0 [get_cells Versal_top_i/flx182_transceivers/gt_quad_base_4/inst/quad_inst]
set_property LOC GTY_REFCLK_X1Y0 [get_cells Versal_top_i/flx182_transceivers/util_ds_buf_4/U0/USE_IBUFDS_GTE5.GEN_IBUFDS_GTE5[0].IBUFDS_GTE5_I]
create_clock -period 3.125 [get_ports bridge_refclkGTY_REFCLK_X1Y0_diff_gt_ref_clock_clk_p[0]]
set_property PACKAGE_PIN AF11 [get_ports bridge_refclkGTY_REFCLK_X1Y0_diff_gt_ref_clock_clk_p]
set_property PACKAGE_PIN AF10 [get_ports bridge_refclkGTY_REFCLK_X1Y0_diff_gt_ref_clock_clk_n]


# quad 201
# refclkGTY_REFCLK_X1Y2 : 25 Gbps with 320 MHz
set_property LOC GTY_QUAD_X1Y1 [get_cells Versal_top_i/flx182_transceivers/gt_quad_base_5/inst/quad_inst]
set_property LOC GTY_REFCLK_X1Y2 [get_cells Versal_top_i/flx182_transceivers/util_ds_buf_5/U0/USE_IBUFDS_GTE5.GEN_IBUFDS_GTE5[0].IBUFDS_GTE5_I]
create_clock -period 3.125 [get_ports bridge_refclkGTY_REFCLK_X1Y2_diff_gt_ref_clock_clk_p[0]]
set_property PACKAGE_PIN AB11 [get_ports bridge_refclkGTY_REFCLK_X1Y2_diff_gt_ref_clock_clk_p]
set_property PACKAGE_PIN AB10 [get_ports bridge_refclkGTY_REFCLK_X1Y2_diff_gt_ref_clock_clk_n]


# quad 202
# refclkGTY_REFCLK_X1Y4 : 25 Gbps with 320 MHz
set_property LOC GTY_QUAD_X1Y2 [get_cells Versal_top_i/flx182_transceivers/gt_quad_base_6/inst/quad_inst]
set_property LOC GTY_REFCLK_X1Y4 [get_cells Versal_top_i/flx182_transceivers/util_ds_buf_6/U0/USE_IBUFDS_GTE5.GEN_IBUFDS_GTE5[0].IBUFDS_GTE5_I]
create_clock -period 3.125 [get_ports bridge_refclkGTY_REFCLK_X1Y4_diff_gt_ref_clock_clk_p[0]]
set_property PACKAGE_PIN L13 [get_ports bridge_refclkGTY_REFCLK_X1Y4_diff_gt_ref_clock_clk_p]
set_property PACKAGE_PIN L12 [get_ports bridge_refclkGTY_REFCLK_X1Y4_diff_gt_ref_clock_clk_n]


# quad 203
# refclkGTY_REFCLK_X1Y6 : 25 Gbps with 320 MHz
set_property LOC GTY_QUAD_X1Y3 [get_cells Versal_top_i/flx182_transceivers/gt_quad_base_7/inst/quad_inst]
set_property LOC GTY_REFCLK_X1Y6 [get_cells Versal_top_i/flx182_transceivers/util_ds_buf_7/U0/USE_IBUFDS_GTE5.GEN_IBUFDS_GTE5[0].IBUFDS_GTE5_I]
create_clock -period 3.125 [get_ports bridge_refclkGTY_REFCLK_X1Y6_diff_gt_ref_clock_clk_p[0]]
set_property PACKAGE_PIN J13 [get_ports bridge_refclkGTY_REFCLK_X1Y6_diff_gt_ref_clock_clk_p]
set_property PACKAGE_PIN J12 [get_ports bridge_refclkGTY_REFCLK_X1Y6_diff_gt_ref_clock_clk_n]


# quad 204
# refclkGTY_REFCLK_X1Y8 : 25 Gbps with 320 MHz
set_property LOC GTY_QUAD_X1Y4 [get_cells Versal_top_i/flx182_transceivers/gt_quad_base_8/inst/quad_inst]
set_property LOC GTY_REFCLK_X1Y8 [get_cells Versal_top_i/flx182_transceivers/util_ds_buf_8/U0/USE_IBUFDS_GTE5.GEN_IBUFDS_GTE5[0].IBUFDS_GTE5_I]
create_clock -period 3.125 [get_ports bridge_refclkGTY_REFCLK_X1Y8_diff_gt_ref_clock_clk_p[0]]
set_property PACKAGE_PIN G13 [get_ports bridge_refclkGTY_REFCLK_X1Y8_diff_gt_ref_clock_clk_p]
set_property PACKAGE_PIN G12 [get_ports bridge_refclkGTY_REFCLK_X1Y8_diff_gt_ref_clock_clk_n]

# quad 205
# refclkGTY_REFCLK_X1Y10 : 25 Gbps with 320 MHz
set_property LOC GTY_QUAD_X1Y5 [get_cells Versal_top_i/flx182_transceivers/gt_quad_base_9/inst/quad_inst]
set_property LOC GTY_REFCLK_X1Y10 [get_cells Versal_top_i/flx182_transceivers/util_ds_buf_9/U0/USE_IBUFDS_GTE5.GEN_IBUFDS_GTE5[0].IBUFDS_GTE5_I]
create_clock -period 3.125 [get_ports bridge_refclkGTY_REFCLK_X1Y10_diff_gt_ref_clock_clk_p[0]]
set_property PACKAGE_PIN E13 [get_ports bridge_refclkGTY_REFCLK_X1Y10_diff_gt_ref_clock_clk_p]
set_property PACKAGE_PIN E12 [get_ports bridge_refclkGTY_REFCLK_X1Y10_diff_gt_ref_clock_clk_n]

# quad 206
# refclkGTY_REFCLK_X1Y12 : 25 Gbps with 320 MHz
set_property LOC GTY_QUAD_X1Y6 [get_cells Versal_top_i/flx182_transceivers/gt_quad_base_10/inst/quad_inst]
set_property LOC GTY_REFCLK_X1Y12 [get_cells Versal_top_i/flx182_transceivers/util_ds_buf_10/U0/USE_IBUFDS_GTE5.GEN_IBUFDS_GTE5[0].IBUFDS_GTE5_I]
create_clock -period 3.125 [get_ports bridge_refclkGTY_REFCLK_X1Y12_diff_gt_ref_clock_clk_p[0]]
set_property PACKAGE_PIN C13 [get_ports bridge_refclkGTY_REFCLK_X1Y12_diff_gt_ref_clock_clk_p]
set_property PACKAGE_PIN C12 [get_ports bridge_refclkGTY_REFCLK_X1Y12_diff_gt_ref_clock_clk_n]

#false path for rate_sel ports
set_false_path -from [get_pins {Versal_top_i/flx182_transceivers/axi_gpio_0/U0/gpio_core_1/Dual.gpio2_Data_Out_reg[*]/C}]