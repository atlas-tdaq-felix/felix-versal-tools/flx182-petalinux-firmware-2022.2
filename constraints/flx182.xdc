# DDRMC pin mapping for FLX-182

set_property PACKAGE_PIN AY37 [get_ports {sys_clk0_0_clk_p[0]}]
set_property PACKAGE_PIN BA37 [get_ports {sys_clk0_0_clk_n[0]}]

set_property PACKAGE_PIN BE35 [get_ports {CH0_DDR4_0_0_adr[0]}]
set_property PACKAGE_PIN BF32 [get_ports {CH0_DDR4_0_0_adr[1]}]
set_property PACKAGE_PIN BB33 [get_ports {CH0_DDR4_0_0_adr[2]}]
set_property PACKAGE_PIN AV34 [get_ports {CH0_DDR4_0_0_adr[3]}]
set_property PACKAGE_PIN AU33 [get_ports {CH0_DDR4_0_0_adr[4]}]
set_property PACKAGE_PIN AP33 [get_ports {CH0_DDR4_0_0_adr[5]}]
set_property PACKAGE_PIN AR32 [get_ports {CH0_DDR4_0_0_adr[6]}]
set_property PACKAGE_PIN BB31 [get_ports {CH0_DDR4_0_0_adr[7]}]
set_property PACKAGE_PIN BG34 [get_ports {CH0_DDR4_0_0_adr[8]}]
set_property PACKAGE_PIN BE32 [get_ports {CH0_DDR4_0_0_adr[9]}]
set_property PACKAGE_PIN BA32 [get_ports {CH0_DDR4_0_0_adr[10]}]
set_property PACKAGE_PIN AP34 [get_ports {CH0_DDR4_0_0_adr[11]}]
set_property PACKAGE_PIN AW31 [get_ports {CH0_DDR4_0_0_adr[12]}]
set_property PACKAGE_PIN BF31 [get_ports {CH0_DDR4_0_0_adr[13]}]
set_property PACKAGE_PIN AT32 [get_ports {CH0_DDR4_0_0_adr[14]}]
set_property PACKAGE_PIN AR33 [get_ports {CH0_DDR4_0_0_adr[15]}]
set_property PACKAGE_PIN BC33 [get_ports {CH0_DDR4_0_0_adr[16]}]
#set_property PACKAGE_PIN BG35 [get_ports {CH0_DDR4_0_0_adr[17]}]
set_property PACKAGE_PIN BD32 [get_ports {CH0_DDR4_0_0_act_n[0]}]
set_property PACKAGE_PIN BA31 [get_ports {CH0_DDR4_0_0_ba[0]}]
set_property PACKAGE_PIN BF34 [get_ports {CH0_DDR4_0_0_ba[1]}]
set_property PACKAGE_PIN AY30 [get_ports {CH0_DDR4_0_0_bg[0]}]
set_property PACKAGE_PIN BE31 [get_ports {CH0_DDR4_0_0_bg[1]}]
set_property PACKAGE_PIN BC31 [get_ports {CH0_DDR4_0_0_ck_t[0]}]
set_property PACKAGE_PIN BD33 [get_ports {CH0_DDR4_0_0_ck_t[1]}]
set_property PACKAGE_PIN BC32 [get_ports {CH0_DDR4_0_0_cke[0]}]
set_property PACKAGE_PIN BG31 [get_ports {CH0_DDR4_0_0_cke[1]}]
set_property PACKAGE_PIN BB34 [get_ports {CH0_DDR4_0_0_cs_n[0]}]
set_property PACKAGE_PIN AW33 [get_ports {CH0_DDR4_0_0_cs_n[1]}]

set_property PACKAGE_PIN AP28 [get_ports {CH0_DDR4_0_0_dqs_t[0]}]
set_property PACKAGE_PIN AU28 [get_ports {CH0_DDR4_0_0_dqs_t[1]}]
set_property PACKAGE_PIN BC28 [get_ports {CH0_DDR4_0_0_dqs_t[2]}]
set_property PACKAGE_PIN AN32 [get_ports {CH0_DDR4_0_0_dqs_t[3]}]
set_property PACKAGE_PIN BF39 [get_ports {CH0_DDR4_0_0_dqs_t[4]}]
set_property PACKAGE_PIN BD39 [get_ports {CH0_DDR4_0_0_dqs_t[5]}]
set_property PACKAGE_PIN AV38 [get_ports {CH0_DDR4_0_0_dqs_t[6]}]
set_property PACKAGE_PIN AT38 [get_ports {CH0_DDR4_0_0_dqs_t[7]}]
#set_property PACKAGE_PIN BF29 [get_ports {CH0_DDR4_0_0_dqs_t[8]}]

set_property PACKAGE_PIN AR27 [get_ports {CH0_DDR4_0_0_dqs_c[0]}]
set_property PACKAGE_PIN AV27 [get_ports {CH0_DDR4_0_0_dqs_c[1]}]
set_property PACKAGE_PIN BD28 [get_ports {CH0_DDR4_0_0_dqs_c[2]}]
set_property PACKAGE_PIN AP31 [get_ports {CH0_DDR4_0_0_dqs_c[3]}]
set_property PACKAGE_PIN BG38 [get_ports {CH0_DDR4_0_0_dqs_c[4]}]
set_property PACKAGE_PIN BD38 [get_ports {CH0_DDR4_0_0_dqs_c[5]}]
set_property PACKAGE_PIN AW37 [get_ports {CH0_DDR4_0_0_dqs_c[6]}]
set_property PACKAGE_PIN AT37 [get_ports {CH0_DDR4_0_0_dqs_c[7]}]
#set_property PACKAGE_PIN BG28 [get_ports {CH0_DDR4_0_0_dqs_c[8]}]

set_property PACKAGE_PIN AR26 [get_ports {CH0_DDR4_0_0_dm_n[0]}]
set_property PACKAGE_PIN AV26 [get_ports {CH0_DDR4_0_0_dm_n[1]}]
set_property PACKAGE_PIN BD27 [get_ports {CH0_DDR4_0_0_dm_n[2]}]
set_property PACKAGE_PIN AR30 [get_ports {CH0_DDR4_0_0_dm_n[3]}]
set_property PACKAGE_PIN BE37 [get_ports {CH0_DDR4_0_0_dm_n[4]}]
set_property PACKAGE_PIN BD37 [get_ports {CH0_DDR4_0_0_dm_n[5]}]
set_property PACKAGE_PIN AW36 [get_ports {CH0_DDR4_0_0_dm_n[6]}]
set_property PACKAGE_PIN AR36 [get_ports {CH0_DDR4_0_0_dm_n[7]}]

set_property PACKAGE_PIN AY38 [get_ports {CH0_DDR4_0_0_reset_n[0]}]
set_property PACKAGE_PIN AV33 [get_ports {CH0_DDR4_0_0_odt[0]}]
set_property PACKAGE_PIN BG33 [get_ports {CH0_DDR4_0_0_odt[1]}]

set_property PACKAGE_PIN AN25 [get_ports {CH0_DDR4_0_0_dq[0]}]
set_property PACKAGE_PIN AN26 [get_ports {CH0_DDR4_0_0_dq[1]}]
set_property PACKAGE_PIN AN28 [get_ports {CH0_DDR4_0_0_dq[2]}]
set_property PACKAGE_PIN AM29 [get_ports {CH0_DDR4_0_0_dq[3]}]
set_property PACKAGE_PIN AM26 [get_ports {CH0_DDR4_0_0_dq[4]}]
set_property PACKAGE_PIN AP25 [get_ports {CH0_DDR4_0_0_dq[5]}]
set_property PACKAGE_PIN AM27 [get_ports {CH0_DDR4_0_0_dq[6]}]
set_property PACKAGE_PIN AN29 [get_ports {CH0_DDR4_0_0_dq[7]}]
set_property PACKAGE_PIN AU25 [get_ports {CH0_DDR4_0_0_dq[8]}]
set_property PACKAGE_PIN AT26 [get_ports {CH0_DDR4_0_0_dq[9]}]
set_property PACKAGE_PIN AU29 [get_ports {CH0_DDR4_0_0_dq[10]}]
set_property PACKAGE_PIN AR29 [get_ports {CH0_DDR4_0_0_dq[11]}]
set_property PACKAGE_PIN AT25 [get_ports {CH0_DDR4_0_0_dq[12]}]
set_property PACKAGE_PIN AV25 [get_ports {CH0_DDR4_0_0_dq[13]}]
set_property PACKAGE_PIN AT28 [get_ports {CH0_DDR4_0_0_dq[14]}]
set_property PACKAGE_PIN AT29 [get_ports {CH0_DDR4_0_0_dq[15]}]
set_property PACKAGE_PIN BB26 [get_ports {CH0_DDR4_0_0_dq[16]}]
set_property PACKAGE_PIN BA28 [get_ports {CH0_DDR4_0_0_dq[17]}]
set_property PACKAGE_PIN BD30 [get_ports {CH0_DDR4_0_0_dq[18]}]
set_property PACKAGE_PIN BB29 [get_ports {CH0_DDR4_0_0_dq[19]}]
set_property PACKAGE_PIN BA27 [get_ports {CH0_DDR4_0_0_dq[20]}]
set_property PACKAGE_PIN BC26 [get_ports {CH0_DDR4_0_0_dq[21]}]
set_property PACKAGE_PIN BD29 [get_ports {CH0_DDR4_0_0_dq[22]}]
set_property PACKAGE_PIN BB28 [get_ports {CH0_DDR4_0_0_dq[23]}]
set_property PACKAGE_PIN AN31 [get_ports {CH0_DDR4_0_0_dq[24]}]
set_property PACKAGE_PIN AV30 [get_ports {CH0_DDR4_0_0_dq[25]}]
set_property PACKAGE_PIN AU31 [get_ports {CH0_DDR4_0_0_dq[26]}]
set_property PACKAGE_PIN AM33 [get_ports {CH0_DDR4_0_0_dq[27]}]
set_property PACKAGE_PIN AM30 [get_ports {CH0_DDR4_0_0_dq[28]}]
set_property PACKAGE_PIN AM32 [get_ports {CH0_DDR4_0_0_dq[29]}]
set_property PACKAGE_PIN AV31 [get_ports {CH0_DDR4_0_0_dq[30]}]
set_property PACKAGE_PIN AT31 [get_ports {CH0_DDR4_0_0_dq[31]}]
set_property PACKAGE_PIN BF36 [get_ports {CH0_DDR4_0_0_dq[32]}]
set_property PACKAGE_PIN BF37 [get_ports {CH0_DDR4_0_0_dq[33]}]
set_property PACKAGE_PIN BG40 [get_ports {CH0_DDR4_0_0_dq[34]}]
set_property PACKAGE_PIN BE40 [get_ports {CH0_DDR4_0_0_dq[35]}]
set_property PACKAGE_PIN BE36 [get_ports {CH0_DDR4_0_0_dq[36]}]
set_property PACKAGE_PIN BG36 [get_ports {CH0_DDR4_0_0_dq[37]}]
set_property PACKAGE_PIN BG39 [get_ports {CH0_DDR4_0_0_dq[38]}]
set_property PACKAGE_PIN BE39 [get_ports {CH0_DDR4_0_0_dq[39]}]
set_property PACKAGE_PIN BC35 [get_ports {CH0_DDR4_0_0_dq[40]}]
set_property PACKAGE_PIN BB36 [get_ports {CH0_DDR4_0_0_dq[41]}]
set_property PACKAGE_PIN BD40 [get_ports {CH0_DDR4_0_0_dq[42]}]
set_property PACKAGE_PIN BC40 [get_ports {CH0_DDR4_0_0_dq[43]}]
set_property PACKAGE_PIN BB35 [get_ports {CH0_DDR4_0_0_dq[44]}]
set_property PACKAGE_PIN BC36 [get_ports {CH0_DDR4_0_0_dq[45]}]
set_property PACKAGE_PIN BC38 [get_ports {CH0_DDR4_0_0_dq[46]}]
set_property PACKAGE_PIN BB38 [get_ports {CH0_DDR4_0_0_dq[47]}]
set_property PACKAGE_PIN AU35 [get_ports {CH0_DDR4_0_0_dq[48]}]
set_property PACKAGE_PIN AY35 [get_ports {CH0_DDR4_0_0_dq[49]}]
set_property PACKAGE_PIN AU37 [get_ports {CH0_DDR4_0_0_dq[50]}]
set_property PACKAGE_PIN AU36 [get_ports {CH0_DDR4_0_0_dq[51]}]
set_property PACKAGE_PIN AW35 [get_ports {CH0_DDR4_0_0_dq[52]}]
set_property PACKAGE_PIN AV35 [get_ports {CH0_DDR4_0_0_dq[53]}]
set_property PACKAGE_PIN AY39 [get_ports {CH0_DDR4_0_0_dq[54]}]
set_property PACKAGE_PIN AW39 [get_ports {CH0_DDR4_0_0_dq[55]}]
set_property PACKAGE_PIN AN35 [get_ports {CH0_DDR4_0_0_dq[56]}]
set_property PACKAGE_PIN AT35 [get_ports {CH0_DDR4_0_0_dq[57]}]
set_property PACKAGE_PIN AM35 [get_ports {CH0_DDR4_0_0_dq[58]}]
set_property PACKAGE_PIN AM36 [get_ports {CH0_DDR4_0_0_dq[59]}]
set_property PACKAGE_PIN AR35 [get_ports {CH0_DDR4_0_0_dq[60]}]
set_property PACKAGE_PIN AP36 [get_ports {CH0_DDR4_0_0_dq[61]}]
set_property PACKAGE_PIN AR37 [get_ports {CH0_DDR4_0_0_dq[62]}]
set_property PACKAGE_PIN AP38 [get_ports {CH0_DDR4_0_0_dq[63]}]


# AXI I2C controller

set_property PACKAGE_PIN L18 [get_ports {IIC_0_scl_io}]
set_property PACKAGE_PIN K17 [get_ports {IIC_0_sda_io}]
set_property IOSTANDARD LVCMOS18 [get_ports {IIC_0_scl_io}]
set_property IOSTANDARD LVCMOS18 [get_ports {IIC_0_sda_io}]

#set_property PACKAGE_PIN A17 [get_ports {IIC_0_scl_io}]
#set_property PACKAGE_PIN B17 [get_ports {IIC_0_sda_io}]
#set_property IOSTANDARD LVCMOS18 [get_ports {IIC_0_scl_io}]
#set_property IOSTANDARD LVCMOS18 [get_ports {IIC_0_sda_io}]


# GPIO constraints

# User Generated IO constraints 

set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[20]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[19]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[18]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[17]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[16]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[15]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[14]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[13]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[12]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[11]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[10]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[9]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[8]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_1_tri_io[0]}]

# User Generated physical constraints 

#SI5345A_SA1
set_property PACKAGE_PIN M37 [get_ports {GPIO_1_tri_io[0]}]
#SI5345B_RST_B
set_property PACKAGE_PIN L37 [get_ports {GPIO_1_tri_io[1]}]
#SI5345A_LOL_B
set_property PACKAGE_PIN N34 [get_ports {GPIO_1_tri_io[2]}]
#SI5345A_FINC_B
set_property PACKAGE_PIN M34 [get_ports {GPIO_1_tri_io[3]}]
#SI5345B_SA0
set_property PACKAGE_PIN M35 [get_ports {GPIO_1_tri_io[4]}]
#SI5345A_FDEC_B
set_property PACKAGE_PIN M36 [get_ports {GPIO_1_tri_io[5]}]
#SI5345B_FDEC_B
set_property PACKAGE_PIN L33 [get_ports {GPIO_1_tri_io[6]}]
#SI5345B_LOL_B
set_property PACKAGE_PIN K33 [get_ports {GPIO_1_tri_io[7]}]
#SI5345A_INTR_B
set_property PACKAGE_PIN K37 [get_ports {GPIO_1_tri_io[8]}]
#SI5345A_OE_B
set_property PACKAGE_PIN J36 [get_ports {GPIO_1_tri_io[9]}]
#SI5345B_SA1
set_property PACKAGE_PIN L34 [get_ports {GPIO_1_tri_io[10]}]
#SI5345B_INTR_B
set_property PACKAGE_PIN K35 [get_ports {GPIO_1_tri_io[11]}]
#SI5345A_SA0
set_property PACKAGE_PIN L35 [get_ports {GPIO_1_tri_io[12]}]
#SI5345B_OE_B
set_property PACKAGE_PIN K36 [get_ports {GPIO_1_tri_io[13]}]
#SI5345B_INSEL0
set_property PACKAGE_PIN J33 [get_ports {GPIO_1_tri_io[14]}]
#SI5345B_INSEL1
set_property PACKAGE_PIN H34 [get_ports {GPIO_1_tri_io[15]}]
#SI5345A_INSEL0
set_property PACKAGE_PIN H36 [get_ports {GPIO_1_tri_io[16]}]
#SI5345A_RST_B
set_property PACKAGE_PIN H37 [get_ports {GPIO_1_tri_io[17]}]
#SI5345B_FINC_B
set_property PACKAGE_PIN J34 [get_ports {GPIO_1_tri_io[18]}]
#SI5345A_INSEL1
set_property PACKAGE_PIN J35 [get_ports {GPIO_1_tri_io[19]}]
#PCIE_PWRBRK
set_property PACKAGE_PIN G36 [get_ports {GPIO_1_tri_io[20]}]

# User Generated IO constraints 

set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[20]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[19]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[18]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[17]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[16]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[15]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[14]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[13]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[12]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[11]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[10]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[9]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[8]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[7]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_0_tri_io[0]}]

# User Generated physical constraints 
#GPIO_LED_2
set_property PACKAGE_PIN M20 [get_ports {GPIO_0_tri_io[0]}]
#I2C_SW_RESET_B
set_property PACKAGE_PIN M21 [get_ports {GPIO_0_tri_io[1]}]
#GPIO_LED_1
set_property PACKAGE_PIN M17 [get_ports {GPIO_0_tri_io[2]}]
#GPIO_LED_0
set_property PACKAGE_PIN L17 [get_ports {GPIO_0_tri_io[3]}]
#GPIO_LED_3
set_property PACKAGE_PIN M19 [get_ports {GPIO_0_tri_io[4]}]
#FAN_FAIL_B
set_property PACKAGE_PIN L19 [get_ports {GPIO_0_tri_io[5]}]
#FAN_FULLSP
set_property PACKAGE_PIN L20 [get_ports {GPIO_0_tri_io[6]}]
#PCIE_PERST_B
set_property PACKAGE_PIN K21 [get_ports {GPIO_0_tri_io[7]}]
#FF3_PRSNT_B
set_property PACKAGE_PIN K18 [get_ports {GPIO_0_tri_io[8]}]
#IOEXPAN1_INTR_B
set_property PACKAGE_PIN J18 [get_ports {GPIO_0_tri_io[9]}]
#FAN_OT_B
set_property PACKAGE_PIN K20 [get_ports {GPIO_0_tri_io[10]}]
#FAN_PWM
set_property PACKAGE_PIN J19 [get_ports {GPIO_0_tri_io[11]}]
#IOEXPAN2_RST_B
set_property PACKAGE_PIN H17 [get_ports {GPIO_0_tri_io[12]}]
#IOEXPAN2_INTR_B
set_property PACKAGE_PIN H18 [get_ports {GPIO_0_tri_io[13]}]
#FAN_TACH
set_property PACKAGE_PIN J20 [get_ports {GPIO_0_tri_io[14]}]
#PCIE_WAKE_B
set_property PACKAGE_PIN J21 [get_ports {GPIO_0_tri_io[15]}]
#IOEXPAN1_RST_B
set_property PACKAGE_PIN H19 [get_ports {GPIO_0_tri_io[16]}]
#QSPI1_RST
set_property PACKAGE_PIN G20 [get_ports {GPIO_0_tri_io[17]}]
#QSPI_RST
set_property PACKAGE_PIN H21 [get_ports {GPIO_0_tri_io[18]}]
#QSPI0_RST
set_property PACKAGE_PIN G21 [get_ports {GPIO_0_tri_io[19]}]
#N/C
set_property PACKAGE_PIN G37 [get_ports {GPIO_0_tri_io[20]}]

set_property IOSTANDARD LVCMOS15 [get_ports {GPIO_1_tri_io[21]}]
set_property IOSTANDARD LVCMOS15 [get_ports {GPIO_1_tri_io[22]}]
set_property IOSTANDARD LVCMOS15 [get_ports {GPIO_1_tri_io[23]}]
set_property IOSTANDARD LVCMOS15 [get_ports {GPIO_1_tri_io[24]}]
#GPIO_DIP_SW0
set_property PACKAGE_PIN BD19 [get_ports {GPIO_1_tri_io[21]}]
#GPIO_DIP_SW1
set_property PACKAGE_PIN BE19 [get_ports {GPIO_1_tri_io[22]}]
#GPIO_DIP_SW2
set_property PACKAGE_PIN BB19 [get_ports {GPIO_1_tri_io[23]}]
#GPIO_DIP_SW3
set_property PACKAGE_PIN BB20 [get_ports {GPIO_1_tri_io[24]}]

#J5 => IN (NIM) => TRG_LVPECL_P/N => TRG_LVDS_P/N
#GPIO2.0[5]
set_property IOSTANDARD LVDS15 [get_ports {trg_in_clk_p[0]}]
set_property IOSTANDARD LVDS15 [get_ports {trg_in_clk_n[0]}]
set_property PACKAGE_PIN AV42 [get_ports {trg_in_clk_p[0]}]
set_property PACKAGE_PIN AV43 [get_ports {trg_in_clk_n[0]}]
#J3 => IN (LVCMCOS33) => WR_10M_IN
#GPIO2.0[6]
set_property IOSTANDARD LVDS15 [get_ports {wr_10m_in_clk_p[0]}]
set_property IOSTANDARD LVDS15 [get_ports {wr_10m_in_clk_n[0]}]
set_property PACKAGE_PIN AY42 [get_ports {wr_10m_in_clk_p[0]}]
set_property PACKAGE_PIN BA43 [get_ports {wr_10m_in_clk_n[0]}]
#J4 => IN (LVCMOS33) => WR_PPS_IN
#GPIO2.0[7]
#[Vivado 12-1411] Cannot set LOC property of ports, Cannot constrain port wr_pps_in_clk_p[0] to site IOB_X22Y0 because it drives fabric resources and there is no fabric connection for this site type in Corner Bank X2Y0.
#set_property IOSTANDARD LVDS15 [get_ports {wr_pps_in_clk_p[0]}]
#set_property IOSTANDARD LVDS15 [get_ports {wr_pps_in_clk_n[0]}]
#set_property PACKAGE_PIN BB44 [get_ports {wr_pps_in_clk_p[0]}]
#set_property PACKAGE_PIN BB45 [get_ports {wr_pps_in_clk_n[0]}]
#J2 => OUT LVCMOS33
#GPIO2.0[8]
#    WR_PPS_OUT_clk_n : out STD_LOGIC_VECTOR ( 0 to 0 );
#    WR_PPS_OUT_clk_p : out STD_LOGIC_VECTOR ( 0 to 0 );
set_property IOSTANDARD LVDS15 [get_ports {WR_PPS_OUT_clk_p[0]}]
set_property IOSTANDARD LVDS15 [get_ports {WR_PPS_OUT_clk_n[0]}]
set_property PACKAGE_PIN BC13 [get_ports {WR_PPS_OUT_clk_p[0]}]
set_property PACKAGE_PIN BD13 [get_ports {WR_PPS_OUT_clk_n[0]}]
#J8 LVCMOS3.3
#GPIO2.0[9]
#    GPIO_BCM_IN_1 : in STD_LOGIC_VECTOR ( 0 to 0 );
set_property IOSTANDARD LVCMOS15 [get_ports {GPIO_BCM_IN_1[0]}]
set_property PACKAGE_PIN AW23 [get_ports {GPIO_BCM_IN_1[0]}]
#J9 LVCMOS3.3
#GPIO2.0[10]
#    GPIO_BCM_IN_2 : in STD_LOGIC_VECTOR ( 0 to 0 );
set_property IOSTANDARD LVCMOS15 [get_ports {GPIO_BCM_IN_2[0]}]
set_property PACKAGE_PIN AV23 [get_ports {GPIO_BCM_IN_2[0]}]
#J6 LVCMOS3.3
#GPIO2.0[11]
#    GPIO_BCM_OUT_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
set_property IOSTANDARD LVCMOS15 [get_ports {GPIO_BCM_OUT_1[0]}]
set_property PACKAGE_PIN BB24 [get_ports {GPIO_BCM_OUT_1[0]}]
#J7 LVCMOS3.3
#GPIO2.0[12]
#    GPIO_BCM_OUT_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
set_property IOSTANDARD LVCMOS15 [get_ports {GPIO_BCM_OUT_2[0]}]
set_property PACKAGE_PIN BA24 [get_ports {GPIO_BCM_OUT_2[0]}]

