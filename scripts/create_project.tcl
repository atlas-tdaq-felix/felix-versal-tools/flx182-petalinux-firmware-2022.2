set PART xcvm1802-vsva2197-1MP-e-S
set PROJECT_NAME FELIX_BIST_firmware
set BD_FILE Versal_top

set scriptdir [pwd]
set firmware_dir $scriptdir/..
set core_dir ${firmware_dir}/sources/bd/
close_project -quiet
create_project -force -part $PART $PROJECT_NAME $firmware_dir/Projects/$PROJECT_NAME
set_property target_language VHDL [current_project]

source ${firmware_dir}/sources/bd/${BD_FILE}.tcl
set WRAPPER_FILE [make_wrapper -files [get_files ${BD_FILE}.bd] -top]
add_files -norecurse $WRAPPER_FILE

read_xdc -verbose ${firmware_dir}/constraints/flx182.xdc
read_xdc -verbose ${firmware_dir}/constraints/versal_ibert.xdc
