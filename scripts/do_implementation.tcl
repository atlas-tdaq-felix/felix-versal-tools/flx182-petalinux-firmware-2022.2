set scriptdir [pwd]
set firmware_dir $scriptdir/..

file mkdir $firmware_dir/output/

#Open block design and copy it into the repository to see if there are any changes
open_bd_design [get_files Versal_top.bd]
validate_bd_design
write_bd_tcl -force ${firmware_dir}/sources/bd/Versal_top.tcl

set GIT_HASH [exec git rev-parse --short HEAD]
set FileName FLX182_BIST_Firmware_${GIT_HASH}
reset_run [get_runs synth_1]
launch_runs [get_runs impl_1] -to_step write_device_image -jobs 8
wait_on_run [get_runs impl_1]
set TOPLEVEL [get_property top [current_fileset]]
set IMPL_DIR [get_property DIRECTORY [get_runs impl_1]]
file copy -force ${IMPL_DIR}/${TOPLEVEL}.pdi $firmware_dir/output/${FileName}.pdi
write_hw_platform -fixed -force $firmware_dir/output/${FileName}.xsa
